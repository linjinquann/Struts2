package com.ljq.struts.action;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;

import java.io.IOException;

/**
 * Created by linjq on 2018/4/1.
 */
//默认可以不写
//@ParentPackage("struts-default")
//根命名空间,可以不写
//@Namespace("/")
//全局配置,如果方法上不指定result,则使用该Result
//@Results({@Result(name="success",location="/success.jsp"),
//    @Result(name="error",location="/error.jsp")})
public class TestAction extends ActionSupport {

    @Action("t1")
    public void t1() throws IOException {
        System.out.println("TestAction t1()...");
        ServletActionContext.getResponse().getWriter().write("TestAction.t1");
        ServletActionContext.getResponse().getWriter().flush();
    }

    @Action("t2")
    public void t2() throws IOException {
        System.out.println("TestAction t2()...");
        ServletActionContext.getResponse().getWriter().write("TestAction.t2");
        ServletActionContext.getResponse().getWriter().flush();
//        return "success";
    }
}
