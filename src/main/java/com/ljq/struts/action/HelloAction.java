package com.ljq.struts.action;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;

import java.io.IOException;

/**
 * Created by linjq on 2018/4/1.
 */
//默认可以不写
//@ParentPackage("struts-default")
//根命名空间,可以不写
//@Namespace("/")
//全局配置,如果方法上不指定result,则使用该Result
//@Results({@Result(name="success",location="/success.jsp"),
//    @Result(name="error",location="/error.jsp")})
public class HelloAction  extends ActionSupport {

    @Action("t1")
    public void t1() throws IOException {
        System.out.println("HelloAction t1()...");
        ServletActionContext.getResponse().getWriter().write("HelloAction.t1");
        ServletActionContext.getResponse().getWriter().flush();
    }

    /**
     * 以下訪問都可以
     * http://localhost:8080/SpringStruts2/hello/t2.action
     * http://localhost:8080/SpringStruts2/hello!t2.action  最好使用该方式，否则多个controller，并且同action，会导致随机访问
     * @Action注解必须得有
     */
    @Action("t2")
    public void t2() throws IOException {
        System.out.println("HelloAction t2()...");
        ServletActionContext.getResponse().getWriter().write("HelloAction.t2");
        ServletActionContext.getResponse().getWriter().flush();
//        return "success";
    }
}
